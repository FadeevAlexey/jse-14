package ru.fadeev.tm.exception;

import org.jetbrains.annotations.Nullable;

public final class IllegalProjectNameException extends RuntimeException {

    public IllegalProjectNameException(@Nullable final String message) {
        super(message);
    }

}
