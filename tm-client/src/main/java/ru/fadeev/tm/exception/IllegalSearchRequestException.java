package ru.fadeev.tm.exception;

import org.jetbrains.annotations.Nullable;

public final class IllegalSearchRequestException extends RuntimeException {

    public IllegalSearchRequestException(@Nullable final String message) {
        super(message);
    }

}
