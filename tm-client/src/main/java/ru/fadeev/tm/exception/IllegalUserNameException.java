package ru.fadeev.tm.exception;

import org.jetbrains.annotations.Nullable;

public final class IllegalUserNameException extends RuntimeException {

    public IllegalUserNameException(@Nullable final String message) {
        super(message);
    }

}
