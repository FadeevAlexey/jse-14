package ru.fadeev.tm.exception;

import org.jetbrains.annotations.Nullable;

public final class InvalidSessionException extends RuntimeException {

    public InvalidSessionException(@Nullable final String message) {
        super(message);
    }

}
