package ru.fadeev.tm.command;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.fadeev.tm.api.service.ServiceLocator;

public abstract class AbstractCommand {

    @Setter
    @NotNull
    protected ServiceLocator serviceLocator;

    public abstract void execute() throws Exception;

    @NotNull
    public abstract String getName();

    @NotNull
    public abstract String getDescription();

}