package ru.fadeev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.command.AbstractCommand;

import java.util.List;

public interface IAppStateService {

    void putCommand(@Nullable String description, @Nullable AbstractCommand abstractCommand);

    @Nullable
    AbstractCommand getCommand(@Nullable String command);

    @NotNull
    List<AbstractCommand> getCommands();

    @Nullable
    String getToken();

    void setToken(@Nullable String token);

}