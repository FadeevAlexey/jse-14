package ru.fadeev.tm.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.util.UUID;

@Getter
@Setter
@ToString
@XmlAccessorType(XmlAccessType.FIELD)
public abstract class AbstractEntityDTO {

    @NotNull
    @XmlElement
    private String id = UUID.randomUUID().toString();

    @NotNull
    public String getId() {
        return id;
    }

}