package ru.fadeev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.repository.ISessionRepository;
import ru.fadeev.tm.api.service.ISessionService;
import ru.fadeev.tm.api.service.ServiceLocator;
import ru.fadeev.tm.dto.SessionDTO;
import ru.fadeev.tm.entity.Session;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.enumerated.Role;
import ru.fadeev.tm.exception.AccessDeniedException;
import ru.fadeev.tm.exception.InvalidSessionException;
import ru.fadeev.tm.repository.SessionRepository;
import ru.fadeev.tm.util.PasswordHashUtil;
import ru.fadeev.tm.util.SignatureUtil;

import javax.persistence.EntityManager;
import java.util.Date;
import java.util.List;

public class SessionService extends AbstractService<Session> implements ISessionService {

    public SessionService(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    public List<Session> findAll() throws Exception {
        @NotNull final EntityManager entityManager = serviceLocator.getSqlSessionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
        @NotNull final List<Session> sessions = repository.findAll();
        entityManager.getTransaction().commit();
        entityManager.close();
        return sessions;
    }

    @Nullable
    @Override
    public Session findOne(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return null;
        @NotNull final EntityManager entityManager = serviceLocator.getSqlSessionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
        @Nullable final Session session = repository.findOne(id);
        entityManager.getTransaction().commit();
        entityManager.close();
        return session;
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return;
        @NotNull final EntityManager entityManager = serviceLocator.getSqlSessionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
        repository.remove(id);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void persist(@Nullable final Session session) throws Exception {
        if (session == null) return;
        @NotNull final EntityManager entityManager = serviceLocator.getSqlSessionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
        repository.persist(session);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void merge(@Nullable final Session session) throws Exception {
        if (session == null) return;
        @NotNull final EntityManager entityManager = serviceLocator.getSqlSessionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
        repository.merge(session);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final EntityManager entityManager = serviceLocator.getSqlSessionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
        repository.removeAll();
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void closeSession(@Nullable final SessionDTO session) throws Exception {
        if (session == null) return;
        if (session.getSignature() == null) return;
        @NotNull final EntityManager entityManager = serviceLocator.getSqlSessionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
        repository.removeSessionBySignature(session.getSignature());
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public boolean contains(@Nullable final String sessionId) throws Exception {
        if (sessionId == null) return false;
        @NotNull final EntityManager entityManager = serviceLocator.getSqlSessionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
        boolean contains = repository.contains(sessionId);
        entityManager.getTransaction().commit();
        entityManager.close();
        return contains;
    }

    @NotNull
    public Session openSession(@Nullable final String login, @Nullable String password) throws Exception {
        if (login == null || login.isEmpty()) throw new InvalidSessionException("Bad login or password");
        if (password == null || password.isEmpty()) throw new InvalidSessionException("Bad login or password");
        @Nullable final User user = serviceLocator.getUserService().findUserByLogin(login);
        if (user == null) throw new InvalidSessionException("Bad login or password");
        if (!PasswordHashUtil.md5(password).equals(user.getPasswordHash()))
            throw new InvalidSessionException("Bad login or password");
        Session session = new Session();
        session.setUser(user);
        session.setRole(user.getRole());
        session.setSignature(SignatureUtil.sign(session,
                serviceLocator.getPropertyService().getSessionSalt(),
                serviceLocator.getPropertyService().getSessionCycle()));
        persist(session);
        return session;
    }

    @NotNull
    public SessionDTO checkSession(final SessionDTO currentSession) throws Exception {
        final long currentTime = new Date().getTime();
        if (currentSession == null) throw new InvalidSessionException();
        if (!contains(currentSession.getId())) throw new InvalidSessionException("Invalid session");
        if (currentSession.getUserId() == null) throw new InvalidSessionException();
        if (currentSession.getSignature() == null) throw new InvalidSessionException();
        if (currentSession.getRole() == null) throw new InvalidSessionException();
        @NotNull final SessionDTO testSession = new SessionDTO();
        testSession.setSignature(currentSession.getSignature());
        testSession.setRole(currentSession.getRole());
        testSession.setId(currentSession.getId());
        testSession.setCreationTime(currentSession.getCreationTime());
        testSession.setUserId(currentSession.getUserId());
        @Nullable final String sessionSignature =
                SignatureUtil.sign(testSession,
                        serviceLocator.getPropertyService().getSessionSalt(),
                        serviceLocator.getPropertyService().getSessionCycle());
        @Nullable final String currentSessionSignature =
                SignatureUtil.sign(currentSession,
                        serviceLocator.getPropertyService().getSessionSalt(),
                        serviceLocator.getPropertyService().getSessionCycle());
        if (sessionSignature == null || currentSessionSignature == null)
            throw new InvalidSessionException("Invalid session");
        if (!sessionSignature.equals(currentSessionSignature)) throw new InvalidSessionException("Invalid session");
        if (currentSession.getCreationTime() - currentTime > serviceLocator.getPropertyService().getSessionLifetime())
            throw new InvalidSessionException("Invalid session");
        return currentSession;
    }

    @NotNull
    public SessionDTO checkSession(final SessionDTO session, @NotNull final Role role) throws Exception {
        if (session == null) throw new InvalidSessionException("Invalid session");
        checkSession(session);
        if (session.getRole() != role) throw new AccessDeniedException("Access denied");
        return session;
    }

}