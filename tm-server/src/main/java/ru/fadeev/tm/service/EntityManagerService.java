package ru.fadeev.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import ru.fadeev.tm.api.service.ISqlSessionService;
import ru.fadeev.tm.api.service.ServiceLocator;
import ru.fadeev.tm.entity.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
public class EntityManagerService implements ISqlSessionService {

    @NotNull
    private final ServiceLocator serviceLocator;

    @NotNull
    private EntityManagerFactory entityManagerFactory;

    public void init() throws Exception {
        entityManagerFactory = factory();
    }

    public EntityManagerService(@NotNull final ServiceLocator serviceLocator)  {
        this.serviceLocator = serviceLocator;
    }

    public EntityManager getEntityManager()  {
        return entityManagerFactory.createEntityManager();
    }

    @NotNull
    private EntityManagerFactory factory() throws Exception {
        final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER, serviceLocator.getPropertyService().getJdbcDriver());
        settings.put(Environment.URL, serviceLocator.getPropertyService().getJdbcUrl());
        settings.put(Environment.USER, serviceLocator.getPropertyService().getJdbcUserName());
        settings.put(Environment.PASS, serviceLocator.getPropertyService().getJdbcPassword());
        settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL5InnoDBDialect");
        settings.put(Environment.HBM2DDL_AUTO, "update");
        settings.put(Environment.SHOW_SQL, "true");
        final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        final StandardServiceRegistry registry = registryBuilder.build();
        final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(User.class);
        sources.addAnnotatedClass(Session.class);
        final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

}