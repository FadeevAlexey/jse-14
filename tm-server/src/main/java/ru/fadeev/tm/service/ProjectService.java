package ru.fadeev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.repository.IProjectRepository;
import ru.fadeev.tm.api.service.IProjectService;
import ru.fadeev.tm.api.service.ServiceLocator;
import ru.fadeev.tm.entity.Project;
import ru.fadeev.tm.repository.ProjectRepository;

import javax.persistence.EntityManager;
import java.util.*;

public final class ProjectService extends AbstractService<Project> implements IProjectService {

    public ProjectService(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    public List<Project> findAll() throws Exception {
        @NotNull final EntityManager entityManager = serviceLocator.getSqlSessionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        @NotNull final List<Project> projects = repository.findAll();
        entityManager.getTransaction().commit();
        entityManager.close();
        return projects;
    }

    @Nullable
    @Override
    public Project findOne(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return null;
        @NotNull final EntityManager entityManager = serviceLocator.getSqlSessionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        @Nullable final Project project = repository.findOne(id);
        entityManager.getTransaction().commit();
        entityManager.close();
        return project;
    }

    @Nullable
    @Override
    public Project findOne(@Nullable final String userId, final String id) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        @NotNull final EntityManager entityManager = serviceLocator.getSqlSessionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        @Nullable Project project = repository.findOneByUserId(userId, id);
        entityManager.getTransaction().commit();
        entityManager.close();
        return project;
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return;
        @NotNull final EntityManager entityManager = serviceLocator.getSqlSessionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        repository.remove(id);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String projectId) throws Exception {
        if (userId == null || userId.isEmpty()) return;
        if (projectId == null || projectId.isEmpty()) return;
        @NotNull final EntityManager entityManager = serviceLocator.getSqlSessionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        repository.removeById(userId, projectId);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void persist(@Nullable final Project project) throws Exception {
        if (project == null) return;
        @NotNull final EntityManager entityManager = serviceLocator.getSqlSessionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        repository.persist(project);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void merge(@Nullable final Project project) throws Exception {
        if (project == null) return;
        @NotNull final EntityManager entityManager = serviceLocator.getSqlSessionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        repository.merge(project);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final EntityManager entityManager = serviceLocator.getSqlSessionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        repository.removeAll();
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Nullable
    @Override
    public String findIdByName(@Nullable final String userId, @Nullable final String name) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        @NotNull final EntityManager entityManager = serviceLocator.getSqlSessionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        @Nullable final String id = repository.findIdByName(userId, name);
        entityManager.getTransaction().commit();
        entityManager.close();
        return id;
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return new ArrayList<>();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlSessionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        @NotNull final List<Project> projects = repository.findAllByUserId(userId);
        entityManager.getTransaction().commit();
        entityManager.close();
        return projects;
    }

    @Override
    public void removeAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return;
        @NotNull final EntityManager entityManager = serviceLocator.getSqlSessionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        repository.removeAllByUserId(userId);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @NotNull
    public Collection<Project> sortByStartDate(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlSessionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        @NotNull final Collection<Project> projects = repository.sortAllByStartDate(userId);
        entityManager.getTransaction().commit();
        entityManager.close();
        return projects;
    }

    @NotNull
    public Collection<Project> sortByFinishDate(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlSessionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        @NotNull final Collection<Project> projects = repository.sortAllByFinishDate(userId);
        entityManager.getTransaction().commit();
        entityManager.close();
        return projects;
    }

    @NotNull
    public Collection<Project> sortByStatus(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlSessionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        @NotNull final Collection<Project> projects = repository.sortAllByStatus(userId);
        entityManager.getTransaction().commit();
        entityManager.close();
        return projects;
    }

    @NotNull
    public Collection<Project> sortByCreationDate(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlSessionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        @NotNull final Collection<Project> projects = repository.sortAllByCreationDate(userId);
        entityManager.getTransaction().commit();
        entityManager.close();
        return projects;
    }

    @Override
    @NotNull
    public Collection<Project> searchByName(@Nullable final String userId, @Nullable final String string) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (string == null || string.isEmpty()) return Collections.emptyList();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlSessionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        @NotNull final Collection<Project> projects = repository.searchByName(userId, string);
        entityManager.getTransaction().commit();
        entityManager.close();
        return projects;
    }

    @Override
    @NotNull
    public Collection<Project> searchByDescription(@Nullable final String userId, @Nullable final String string) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (string == null || string.isEmpty()) return Collections.emptyList();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlSessionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        @NotNull final Collection<Project> projects = repository.searchByDescription(userId, string);
        entityManager.getTransaction().commit();
        entityManager.close();
        return projects;
    }

}