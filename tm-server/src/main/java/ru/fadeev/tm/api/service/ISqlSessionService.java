package ru.fadeev.tm.api.service;


import javax.persistence.EntityManager;

public interface ISqlSessionService {

    EntityManager getEntityManager() throws Exception;

    void init() throws Exception;

}
