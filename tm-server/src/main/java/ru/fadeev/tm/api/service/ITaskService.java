package ru.fadeev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.entity.Task;

import java.util.Collection;

public interface ITaskService extends IService<Task> {

    @Nullable
    String findIdByName(@Nullable String userId, @Nullable String name) throws Exception;

    @NotNull
    Collection<Task> findAll(@Nullable String userId) throws Exception;

    void removeAll(@Nullable String userId) throws Exception;

    @NotNull
    Collection<Task> sortByStartDate(@Nullable  String userId) throws Exception;

    @NotNull
    Collection<Task> sortByFinishDate(@Nullable final String userId) throws Exception;

    @NotNull
    Collection<Task> sortByStatus(@Nullable final String userId) throws Exception;

    @NotNull
    Collection<Task> sortByCreationDate(@Nullable final String userId) throws Exception;

    @NotNull
    Collection<Task> searchByName(@Nullable String userId, @Nullable String string) throws Exception;

    @NotNull
    Collection<Task> searchByDescription(@Nullable String userId, @Nullable String string) throws Exception;

    @NotNull
    Collection<Task> findAllByProjectId(@Nullable String projectId, @Nullable String userId) throws Exception;

    void removeAllByProjectId(@Nullable String userId, @Nullable String projectId) throws Exception;

    void removeAllProjectTask(@Nullable String userId) throws Exception;

    @Nullable
    Task findOne(@Nullable String userId, String id) throws Exception;

    void remove(@Nullable String userId, @Nullable String projectId) throws Exception;

}