package ru.fadeev.tm.entity;

import com.fasterxml.jackson.annotation.JsonFilter;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.enumerated.Role;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@Entity(name = "app_session")
public class Session extends AbstractEntity {

    @Nullable
    @ManyToOne
    @JsonFilter("idFilter")
    private User user;

    @Nullable
    private String signature;

    @Column(columnDefinition = "BIGINT", updatable = false)
    private long creationTime = new Date().getTime();

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Role role;

}