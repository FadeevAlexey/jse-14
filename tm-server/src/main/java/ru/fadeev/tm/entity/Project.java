package ru.fadeev.tm.entity;

import lombok.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.enumerated.Status;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity(name = "app_project")
public final class Project extends AbstractEntity {

    @NotNull
    @ManyToOne
    private User user;

    @Nullable
    @OneToMany(
            mappedBy = "project",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Task> tasks = new ArrayList<>();

    @NotNull
    @Column(nullable = false)
    private String name = "";

    @Nullable
    @Column(columnDefinition = "TEXT")
    private String description = "";

    @Nullable
    private Date startDate;

    @Nullable
    private Date finishDate;

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Status status = Status.PLANNED;

    @NotNull
    @Column(updatable = false)
    private Date creationTime = new Date(System.currentTimeMillis());

}